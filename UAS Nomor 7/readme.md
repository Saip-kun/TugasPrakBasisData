#Procedure / Function dan Trigger
## Function
![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-09%20005435.png )

Dalam kodingan yang diberikan, terdapat dua fungsi yang umum digunakan dalam skrip migrasi Laravel, yaitu up() dan down(). Namun, tidak ada penggunaan trigger dalam kodingan tersebut.

up(): Fungsi up() digunakan untuk mendefinisikan perubahan atau pembuatan skema basis data yang dijalankan ketika migrasi dijalankan atau diterapkan. Dalam contoh tersebut, fungsi up() digunakan untuk membuat tabel users dengan kolom-kolom yang sesuai.

down(): Fungsi down() digunakan untuk mendefinisikan perubahan atau pembuatan skema basis data yang akan dijalankan ketika migrasi di-rollback atau dihapus. Dalam contoh tersebut, fungsi down() digunakan untuk menghapus tabel users.

Dalam contoh migrasi tersebut, tabel users dibuat dengan menggunakan Schema::create(). Di dalam closure function yang diberikan kepada Schema::create(), Blueprint $table digunakan untuk mendefinisikan struktur kolom dalam tabel.

## Trigger

Dalam kerangka kerja Laravel, penggunaan Trigger pada basis data umumnya dilakukan secara langsung melalui DBMS (Database Management System) yang digunakan, seperti MySQL, PostgreSQL, atau Oracle. Laravel sendiri tidak menyediakan fitur khusus untuk mendefinisikan Trigger secara langsung dalam kode Laravel.

Namun, meskipun Laravel tidak menyediakan cara langsung untuk membuat Trigger dalam kode Laravel, Anda masih dapat menggunakan fitur Query Builder atau Eksekusi Query Langsung untuk membuat dan mengelola Trigger dalam basis data.
