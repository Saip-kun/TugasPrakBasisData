#Mendemonstrasikan penggunaan Minimal 3 Built-in Function
Perintah yang diberikan yaitu mendemonstrasikan penggunaan built-in Function dengana ketentuan:
- RegEx
- SubString
- Sisanya bebas

Namun sayangnya saya belum mencapai tahapan yang membuat saya membutuhkan menggunakan Function RegEx. Sehingga saya hanya akan memaparkan Built-in Function yang saya gunakan saja.

## SubString
![image.png](https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-09%20002142.png)

Pada kode yang diberikan, terdapat fungsi `generateToken()` yang bertujuan untuk menghasilkan token acak dengan panjang 60 karakter.

Berikut adalah penjelasan mengenai penggunaan fungsi substring pada kode tersebut:

1. Variabel `$allCart`: Variabel ini menggabungkan karakter alfabet (huruf kecil dan huruf besar) dan karakter numerik menjadi satu string. String ini akan digunakan sebagai basis karakter untuk token.

2. `str_shuffle($allCart)`: Fungsi `str_shuffle()` digunakan untuk mengacak urutan karakter dalam string `$allCart`. Fungsi ini akan menghasilkan urutan karakter yang acak setiap kali dipanggil.

3. `substr(str_shuffle($allCart), 0, 60)`: Fungsi `substr()` digunakan untuk mengambil sebagian substring dari string yang dihasilkan dari `str_shuffle($allCart)`. Pada kode ini, kita mengambil 60 karakter pertama dari string acak tersebut, yang pada akhirnya akan menjadi token acak dengan panjang 60 karakter.

Dengan menggunakan `substr()` dan `str_shuffle()`, fungsi `generateToken()` menghasilkan token acak yang terdiri dari karakter alfabet (huruf kecil dan huruf besar) dan karakter numerik dengan panjang 60 karakter. Token ini dapat digunakan sebagai identifikasi atau tanda pengenal unik untuk proses otentikasi atau keperluan keamanan lainnya dalam aplikasi.

## Validator
![image.png](https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-09%20000300.png)

Pada kode yang diberikan, terdapat penggunaan fungsi Validator untuk melakukan validasi data input dalam fungsi `login()` dan `register()`.

Pada fungsi `login()`, Validator digunakan untuk memvalidasi input email dan password yang diterima dari `$request`. Aturan validasi yang diberikan adalah:

- `email` harus diisi (required).
- `password` harus diisi dan memiliki panjang minimal 6 karakter (required|min:6).

Jika validasi gagal, yaitu jika ada error yang terdeteksi, fungsi `fails()` akan mengembalikan nilai `true`, dan `error()` akan dipanggil untuk mengembalikan pesan error pertama yang ditemukan melalui `$validasi->errors()->first()`. Pesan error ini kemudian dikembalikan sebagai respons dari fungsi.

Pada fungsi `register()`, Validator juga digunakan untuk memvalidasi data input dari `$request`. Aturan validasi yang diberikan adalah:

- `name` harus diisi (required).
- `email` harus diisi dan harus unik dalam tabel `users` (required|unique:users).
- `phone` harus diisi dan harus unik dalam tabel `users` (required|unique:users).
- `password` harus diisi dan memiliki panjang minimal 6 karakter (required|min:6).

Jika validasi gagal, fungsi `fails()` akan mengembalikan nilai `true`, dan `error()` akan dipanggil untuk mengembalikan pesan error pertama yang ditemukan melalui `$validasi->errors()->first()`. Pesan error ini kemudian dikembalikan sebagai respons dari fungsi.

Dengan menggunakan Validator, kedua fungsi `login()` dan `register()` dapat memvalidasi data input sebelum melanjutkan dengan proses login atau registrasi. Ini membantu memastikan bahwa data yang masuk memenuhi kriteria yang diinginkan sebelum diproses lebih lanjut.

## HTTP
![image.png](https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-07%20091110.png)

Dalam kode yang Anda berikan, terdapat penggunaan beberapa fungsi bawaan Laravel yang berhubungan dengan routing (rute). Berikut adalah penjelasan mengenai beberapa fungsi bawaan yang digunakan dalam kode tersebut:

1. `get()` dan `post()`: Fungsi ini digunakan untuk mendefinisikan rute yang akan menangani permintaan HTTP dengan metode GET atau POST. Misalnya, `get('/user')` digunakan untuk mendefinisikan rute yang menangani permintaan GET pada URL '/user', dan `post('login')` digunakan untuk mendefinisikan rute yang menangani permintaan POST pada URL '/login'.

2. `middleware()`: Fungsi ini digunakan untuk mendaftarkan middleware pada grup rute tertentu. Middleware adalah lapisan perantara antara permintaan dan respons dalam aplikasi Laravel. Misalnya, `middleware('auth:sanctum')` digunakan untuk menerapkan middleware 'auth:sanctum' pada rute '/user'. Ini berarti rute tersebut hanya dapat diakses oleh pengguna yang telah terautentikasi menggunakan Sanctum.

3. `group()`: Fungsi ini digunakan untuk mengelompokkan rute-rute yang memiliki middleware yang sama atau menggunakan pengaturan yang sama. Dalam contoh kode yang diberikan, terdapat dua grup rute yang dikelompokkan berdasarkan middleware yang berbeda ('user' dan 'admin'). Rute-rute yang didefinisikan di dalam grup tersebut akan mewarisi middleware dan pengaturan dari grup tersebut.

4. `resource()`: Fungsi ini digunakan untuk menghasilkan rute dengan metode CRUD (Create, Read, Update, Delete) untuk sumber daya (resource) tertentu. Misalnya, `resource('toko', TokoController::class)` akan menghasilkan rute-rute standar untuk mengelola sumber daya 'toko', termasuk rute untuk menampilkan daftar, menampilkan form tambah, menyimpan data, menampilkan detail, menampilkan form edit, memperbarui data, dan menghapus data.

Dengan menggunakan fungsi-fungsi bawaan ini, Anda dapat dengan mudah mendefinisikan rute-rute yang akan menangani permintaan HTTP pada aplikasi Laravel. Rute-rute ini akan terhubung dengan fungsi-fungsi yang ditentukan dalam kontroler yang terkait, yang akan menjalankan logika bisnis atau logika aksi yang diperlukan sesuai dengan permintaan yang diterima.
