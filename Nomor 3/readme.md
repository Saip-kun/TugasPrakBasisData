## Create
```membuat table```
<pre><code>
create database tokopedia;

CREATE TABLE kategori (
  id INT PRIMARY KEY,
  nama VARCHAR(255),
  deskripsi TEXT
);

CREATE TABLE merek (
  id INT PRIMARY KEY,
  nama VARCHAR(255)
);

CREATE TABLE produk (
  id INT PRIMARY KEY,
  nama VARCHAR(255),
  harga INT,
  deskripsi TEXT,
  kategori_id INT,
  merek_id INT,
  kondisi VARCHAR(255),
  berat INT,
  stok INT,
  FOREIGN KEY (kategori_id) REFERENCES kategori(id),
  FOREIGN KEY (merek_id) REFERENCES merek(id)
);

CREATE TABLE transaksi (
  id INT PRIMARY KEY,
  pengguna_id INT,
  tanggal_transaksi DATE,
  total_harga INT,
  metode_pembayaran VARCHAR(255),
  status VARCHAR(255),
  FOREIGN KEY (pengguna_id) REFERENCES pengguna(id)
);

CREATE TABLE produk_transaksi (
  transaksi_id INT,
  produk_id INT,
  kuantitas INT,
  FOREIGN KEY (transaksi_id) REFERENCES transaksi(id),
  FOREIGN KEY (produk_id) REFERENCES produk(id)
);

CREATE TABLE pengguna (
  id INT PRIMARY KEY,
  nama VARCHAR(255),
  email VARCHAR(255),
  alamat VARCHAR(255),
  no_telp VARCHAR(255)
);

CREATE TABLE alamat (
  id INT,
  nama VARCHAR(255),
  alamat VARCHAR(255),
  kecamatan VARCHAR(255),
  kota VARCHAR(255),
  kode_post VARCHAR(255),
  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES pengguna(id)
);

CREATE TABLE pengiriman (
  id INT PRIMARY KEY,
  transaksi_id INT,
  alamat_pengiriman VARCHAR(255),
  tanggal_pengiriman DATE,
  tanggal_penerimaan DATE,
  FOREIGN KEY (transaksi_id) REFERENCES transaksi(id)
);

CREATE TABLE ulasan_produk (
  id INT PRIMARY KEY,
  produk_id INT,
  pengguna_id INT,
  tanggal_ulasan DATE,
  rating INT,
  komentar TEXT,
  FOREIGN KEY (produk_id) REFERENCES produk(id),
  FOREIGN KEY (pengguna_id) REFERENCES pengguna(id)
);

CREATE TABLE wishlist (
  id INT PRIMARY KEY,
  pengguna_id INT,
  produk_id INT,
  FOREIGN KEY (pengguna_id) REFERENCES pengguna(id),
  FOREIGN KEY (produk_id) REFERENCES produk(id)
);

</pre></code>

## Insert
```penggunas```

<pre><code>INSERT INTO pengguna (id, nama, email, alamat, no_telp)
VALUES 
(1, 'Teuku Muhammad Saif', 'saifsyafii22@gmail.com', 'Alamat 1', '082374535639'),
(2, 'teuku m saif', 'chana3034@gmail.com', 'Alamat 2', '088218686514'),
(3, 'Eli Fazlan', 'eli@gmail.com', 'Alamat 3', '088218686513'),
(4, 'Yuda Ristian', 'yuda@gmail.com', 'Alamat 4', '088335468123'),
(5, 'Ihchsanul Kamil', 'ichsanul@gmail.com', 'Alamat 5', '088335468124'),
(6, 'Ahmad', 'ahmad@gmail.com', 'Alamat 6', '085298654125'),
(7, 'Ahmad', 'ahmad1@gmail.com', 'Alamat 7', '085298654122'),
(8, 'Ulul Abshar', 'ulul@gmail.com', 'Alamat 8', '082356897412'),
(9, 'Ridi Rinanda', 'ridi@gmail.com', 'Alamat 9', '085219374685'),
(10, 'Elshadra Righayatsyah', 'elshadra@gmail.com', 'Alamat 10', '088216648569'),
(11, 'Yazid Bayhaqi', 'yazid@gmail.com', 'Alamat 11', '089586724553'),
(12, 'Hasbul Hadi', 'hasbul@gmail.com', 'Alamat 12', '089564872213'),
(13, 'Aqil Vathani', 'aqilvathani@gmail.com', 'Alamat 13', '081255896432'),
(14, 'Rafi Kamaul', 'rafi@gmail.com', 'Alamat 14', '085344856956'),
(15, 'Aqil Abrar', 'aqilabrar@gmail.com', 'Alamat 15', '082174458995'),
(16, 'Riski Alfita', 'riskialfita@gmail.com', 'Alamat 16', '082144665588'),
(17, 'Syaukat', 'syaukat@gmail.com', 'Alamat 17', '088544532677'),
(18, 'Rizki Dwi Putra', 'rizkidwi@gmail.com', 'Alamat 18', '085244695599'),
(19, 'Afif Shadiq', 'afifshadiq@gmail.com', 'Alamat 19', '0889545632154'),
(20, 'Andika Maula', 'andikamaula@gmail.com', 'Alamat 20', '085364978821');
</pre></code>

```produks```
<pre><code>INSERT INTO produk 
(id, nama, harga, deskripsi, kategori_id, merek_id, kondisi, berat, stok)
VALUES
(1, 'mie sedap kari spesial', 3000, 'Deskripsi Mie Sedap Kari Spesial', 111, 20111, 'new', 300, 50),
(2, 'mie sedap soto medan', 3000, 'Deskripsi Mie Sedap Soto Medan', 111, 20111, 'new', 300, 45),
(3, 'mie sedap korean spicy', 3000, 'Deskripsi Mie Sedap Korean Spicy', 111, 20111, 'new', 300, 50),
(4, 'teh botol', 6000, 'Deskripsi Teh Botol', 211, 30111, 'new', 600, 120),
(5, 'fruitea', 5000, 'Deskripsi Fruitea', 211, 30111, 'new', 600, 120),
(6, 'teh poci', 3000, 'Deskripsi Teh Poci', 212, 31111, 'new', 600, 120);
</pre></code>

```merek```
<pre><code> INSERT INTO merek (id, nama)
VALUES
(20111,'Mie Sedap'),
(21111,'Indomie'),
(30111,'Sosro'),
(31111,'Poci Kreasi');
</pre></code>


## UPDATE
<pre><code>UPDATE penggunas
SET nama = 'Ahmad Zaki'
WHERE id = 6;

UPDATE penggunas
SET nama = 'Ahmad Shiddiq'
WHERE id = 7;

UPDATE penggunas
SET email = 'ahmadzaki@gmail.com'
WHERE id = 6;

UPDATE penggunas
SET nama = 'ahmadshiddiq@gmail.com'
WHERE id = 7;
</pre></code>


## READ
Show Entire Table Column
<pre><code>SELECT * FROM penggunas;
</pre></code>

Show any Column From Table
<pre><code>SELECT id, nama, email
FROM penggunas;
</pre></code>

Show ROW by Condition From Table
<pre><code>SELECT * FROM penggunas
WHERE id > 4;
</pre></code>

## DELETE
<pre><code>DELETE FROM penggunas
WHERE id = 20;
</pre></code>
