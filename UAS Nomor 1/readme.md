# SUBQUERY
## Pengertian
Subquery atau kadang disebut juga dengan nested queries atau inner queries merupakan query yang terletak di dalam query utama (main query/outer query). 

Query utama bersifat dependen dengan query di dalamnya. Artinya, hasil yang dikeluarkan dari query utama bergantung dengan hasil yang diperoleh dari subquery. Sedangkan subquery dapat bersifat independen (kecuali correlated subquery) yang artinya dapat dijalankan dengan sendirinya.


## Scalar Subquery
``` Pada scalar subquery, hasil yang diperoleh berupa nilai tunggal. Umumnya, nilai ini kemudian digunakan pada klausa SELECT, WHERE, ataupun HAVING untuk membandingkan data yang ada pada query utama dengan nilai tunggal yang dihasilkan dari subquery.```
<pre><code>SELECT nama, harga, merek_id
FROM produk
WHERE harga > (SELECT avg (harga) FROM produk);
</pre></code>
![image.png](https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-07%20073212.png)

## Single-Row Subquery
``` Single row subquery merupakan jenis subquery yang menghasilkan data dalam bentuk satu baris. Pada single row subquery, hasil yang diperoleh digunakan sebagai operator pembanding seperti =, >, <, atau klausa IN.```
<pre><code>SELECT nama, harga, merek_id
FROM produk
WHERE merek_id = (SELECT id FROM merek WHERE id = 30111);
</pre></code>
![image.png](https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-07%20075251.png)

## Multiple-Row Subquery
``` Multiple row subquery merupakan jenis subquery yang menghasilkan beberapa baris data. Serupa dengan single row subquery, hasil yang diperoleh dari subquery akan digunakan sebagai operator pembanding. Akan tetapi, dikarenakan hasilnya merupakan gabungan dari beberapa data, klausa yang dapat digunakan hanya klausa IN atau NOT IN.```
<pre><code>SELECT nama, harga, merek_id
FROM produk
WHERE merek_id IN (SELECT DISTINCT id FROM merek);
</pre></code>
![image.png](https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-07%20081256.png)

