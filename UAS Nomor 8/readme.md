# DCL (Data Control Language)
Data Control Language (DCL) adalah salah satu dari kelompok perintah SQL yang digunakan untuk melakukan kontrol terhadap privilege atau hak akses khusus untuk berinteraksi dengan database. Hak akses khusus ini diperlukan sebagai prasyarat bagi setiap user database untuk melakukan berbagai aksi di database, seperti: membuat object, menghapus object, mengubah object, menampilkan hasil query, dan seterusnya. Sederhananya, setiap pengguna database hanya dapat melakukan aksi-aksi yang sudah diberikan oleh user dengan kontrol tertinggi di dalam database tersebut.

## DCL Pada PHP Laravel
Dalam kerangka kerja Laravel, tidak selalu diperlukan untuk mencantumkan atau menggunakan perintah Data Control Language (DCL) secara eksplisit. Penggunaan DCL sangat tergantung pada kebutuhan aplikasi dan skenario yang sedang Anda hadapi.

DCL digunakan untuk mengatur hak akses dan keamanan dalam basis data, seperti memberikan izin akses, mencabut izin, atau mengelola kebijakan keamanan. Penggunaan DCL biasanya terjadi pada tahap pengaturan dan pengelolaan basis data, dan dapat melibatkan skrip migrasi, eksekusi query langsung, atau interaksi dengan objek basis data.

Namun, dalam banyak kasus, Laravel menyediakan fitur yang lebih tinggi tingkat, seperti Eloquent ORM dan Query Builder, yang memungkinkan Anda berinteraksi dengan basis data menggunakan operasi objek dan metode tingkat tinggi, yang secara otomatis mengelola hak akses dan kebijakan keamanan dalam konteks yang lebih abstrak.

Jadi, kebutuhan untuk mencantumkan DCL secara eksplisit dalam kode Anda sangat tergantung pada kebutuhan dan skenario aplikasi yang sedang Anda bangun. Jika Anda tidak memiliki kebutuhan khusus untuk mengatur hak akses atau kebijakan keamanan dalam basis data, maka DCL mungkin tidak perlu dicantumkan dalam kode Laravel Anda.

## Query Builder Pada DB Saya
![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-09%20000300.png )

Pada kode diatas, terdapat penggunaan Query Builder dalam beberapa metode pada kontroler. 
Berikut ini adalah penjelasan mengenai penggunaan Query Builder pada masing-masing metode:
1. Metode `login`:
   - Pada baris `$validasi = Validator::make($request->all(), [...])`, dilakukan validasi input menggunakan Validator.
   - Pada baris `$user = User::where('email', $request->email)->with('userRole')->first();`, digunakan Query Builder untuk mengambil data pengguna berdasarkan alamat email yang diberikan. Metode `with('userRole')` digunakan untuk memuat relasi 'userRole' dari model User.
   - Pada baris `if (password_verify($request->password, $user->password)) { ... }`, dilakukan verifikasi password menggunakan fungsi `password_verify()`.
   - Pada baris `$token = PersonalToken::create([...])`, digunakan Query Builder untuk membuat token personal baru pada model PersonalToken.
   - Pada baris `$user->update([...])`, dilakukan pembaruan atribut 'token' pada model User.
   - Pada baris `return $this->success($user);`, mengembalikan respon sukses dengan data pengguna yang berhasil login.

2. Metode `register`:
   - Pada baris `$validasi = Validator::make($request->all(), [...])`, dilakukan validasi input menggunakan Validator.
   - Pada baris `$user = User::create([...])`, digunakan Query Builder untuk membuat entri baru pada tabel users berdasarkan data yang diberikan.
   - Pada baris `return $this->success($user, 'selamat datang ' . $user->name);`, mengembalikan respon sukses dengan data pengguna yang berhasil terdaftar.

3. Metode `update`:
   - Pada baris `$user = User::where('id', $id)->first();`, digunakan Query Builder untuk mengambil data pengguna berdasarkan ID yang diberikan.
   - Pada baris `$user->update($request->all());`, dilakukan pembaruan atribut pada model User berdasarkan data yang diberikan.

4. Metode `upload`:
   - Pada baris `$user = User::where('id', $id)->first();`, digunakan Query Builder untuk mengambil data pengguna berdasarkan ID yang diberikan.
   - Pada baris `$user->update([...])`, dilakukan pembaruan atribut 'image' pada model User.
   - Pada baris `$request->image->storeAs('public/user', $image);`, menggunakan metode `storeAs()` untuk menyimpan file gambar yang diunggah ke lokasi yang ditentukan.

Dalam penggunaan Query Builder pada metode-metode tersebut, Anda dapat melihat penggunaan metode seperti `where()`, `first()`, `create()`, dan `update()` untuk melakukan operasi pengambilan, pembuatan, dan pembaruan data pada tabel users. Metode tersebut memungkinkan Anda untuk membangun query SQL dengan cara yang lebih ekspresif dan mudah dibaca daripada menulis query SQL langsung.

Sama halnya dngen menggunakan DCL, Query Builder pada Laravel juga menangani sanitasi parameter, sehingga membantu mencegah serangan SQL Injection dan memastikan keamanan aplikasi.
