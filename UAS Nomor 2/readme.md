# DATABASE TRANSACTION

## A. Pengertian
Database transaction adalah salah satu subset yang disediakan SQL. Gunanya untuk mengatur alur data transaksi dalam suatu database. Transaction ini biasanya digunakan didalam trigger. Namun, jangan samakan transaction ini dengan trigger. Trigger dijalankan untuk menanggapi perubahan tertentu pada tabel tertentu.

## B. Konsep Database Transaction
Karena kasus sejenis itu, para ilmuwan dunia “Juru Ketik” membuat yang disebut proses Transaction. Dengan ini, jika ada kegagalan dalam 1 node (proses), seluruh transaksi (proses input) akan dibatalkan. Sementara itu, jika berhasil akan di-commit.

Transaction terdiri atas hal-hal berikut:
```BEGIN```
digunakan saat data akan diproses.
```COMMIT```
digunakan jika semua kasus pada alur proses berhasil. Bila demikian, fungsi commit harus diterapkan agar tersimpan di database.
```ROLLBACK```
digunakan jika terdapat kasus ada kegagalan (kesalahan/error) di salah satu proses itu. Maka, semua proses input akan dibatalkan (rollback) dan tidak akan disimpan ke dalam database.

## C. Database Transaction laravel
Di Laravel Terdapat 3 metode yang biasa digunakan saat menggunakan DB transaction yaitu :

```1. DB::beginTransactions```
Kalian dapat menggunakan metode ini untuk memulai transaksi pada permulaan statement


```2. DB::commit```
Metode ini digunakan untuk menyimpan semua operasi yang dilakukan setelah metode DB::beginTransactions


```3. DB::rollback```
Metode ini digunakan untuk membatalkan semua operasi yang dilakukan setelah metode DB::beginTransaction
