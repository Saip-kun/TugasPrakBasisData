# Constraint
## Pengertian
Constraints adalah batasan-batasan yang diberikan untuk tiap kolom. sebagainya. Ini dapat digunakan untuk mendefinisikan kunci primer (primary key) dan kunci tamu (foreign key).

## Cara Kerja Constraint
Cara kerja sebuah constraint atau batasan adalah dengan mendefinisikan sebuah aturan constraint pada level tabel atau pada umumnya ditentukan pada sebuah kolom. Dengan menggunakan fungsi constraint ini, kita dapat menjaga konsistensi data dan validitas dari data itu sendiri. 

## Constraint pada laravel
Dalam kerangka kerja Laravel, Anda dapat menggunakan fitur Constraint dalam konteks basis data. Laravel menyediakan berbagai cara untuk mendefinisikan constraint pada skema basis data, termasuk penggunaan migrasi dan Fluent Query Builder.

## Constrain yang digunakan
![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-09%20145117.png )

Pada kodingan migrasi diatas, terdapat beberapa constraint yang diterapkan pada tabel 'users'. 
1. Primary Key:
   - Pada baris `$table->id();`, digunakan untuk membuat kolom 'id' sebagai primary key tabel 'users'. Primary key adalah kolom yang secara unik mengidentifikasi setiap baris dalam tabel.

2. Unique Constraint:
   - Pada baris `$table->string('email')->unique();`, digunakan untuk membuat kolom 'email' dengan constraint unique. Constraint unique memastikan bahwa setiap nilai dalam kolom 'email' harus unik, sehingga tidak ada dua pengguna dengan alamat email yang sama dalam tabel.

   - Pada baris `$table->string('phone')->unique();`, digunakan untuk membuat kolom 'phone' dengan constraint unique. Constraint unique memastikan bahwa setiap nilai dalam kolom 'phone' harus unik, sehingga tidak ada dua pengguna dengan nomor telepon yang sama dalam tabel.

![image.png]( https://raw.githubusercontent.com/teukumsaip/gambar/main/Screenshot%202023-07-09%20143414.png )

3. Index Constraint:
   - Pada baris `$table->string('email')->index();`, digunakan untuk membuat kolom 'email' dengan constraint index. Index constraint digunakan untuk meningkatkan performa pencarian dan pengurutan data dalam kolom. Dengan adanya index pada kolom 'email', query yang melibatkan pencarian berdasarkan kolom ini akan menjadi lebih cepat.

4. Not Null Constraint:
   - Tidak ada pernyataan not null yang secara eksplisit diterapkan dalam kodingan tersebut. Namun, secara default, kolom yang didefinisikan menggunakan `$table->string()` dianggap nullable. Artinya, kolom 'email' dan 'token' akan diizinkan untuk memiliki nilai null kecuali dinyatakan sebaliknya.
